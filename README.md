No : Nobelium
==============

The project is now [here](http://dev.yeuxdelibad.net/tk-tools/No)

No is a simple notifier which doesn't depend on any huge graphic library (no GTK, no Qt). It is useful in scripts or any purpose.

	no --title "Nouveau message" --details "Nouveau message de la part d'un inconnu" --icon /usr/local/share/icons/Adwaita/48x48/emotes/face-surprise.png

![screenshot of nobelium 1](no1.png)

	no --title "Notification" --details "Message important en rouge" --background red --icon /usr/local/share/icons/Adwaita/48x48/emblems/emblem-urgent.png

![screenshot of nobelium 1](no2.png)

<video src="no3.webm" controls></video>

Dependencies
------------

You'll need python-3.* and only these libraries : 

- python-tkinter
- python-PIL or python-Pillow


Usage
-----

	usage: no [-h] --title TITLE [--details DETAILS] [--icon ICON]
				[--position {NW,N,NE,E,SE,S,SW,W}] [--duration DURATION]
				[--animate] [--margin MARGIN] [--background BACKGROUND]
				[--foreground FOREGROUND]

	Simple notifier

	optional arguments:
	-h, --help            show this help message and exit
	--title TITLE         Set the notification title
	--details DETAILS     Set the longer message to show
	--icon ICON           Set the image path to use as icon
	--position {NW,N,NE,E,SE,S,SW,W}
							Set the notification position.
	--duration DURATION   Set the duration while notification is shown
	--animate             Enable notification animation
	--margin MARGIN       Margin to screen border
	--background BACKGROUND
							Background color
	--foreground FOREGROUND
							Foreground color

